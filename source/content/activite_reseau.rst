Activité : Réseau et adressage IP
==================================

Couches réseau et protocoles
------------------------------

La communication entre 2 machines se réalise grâce à différents éléments physiques et protocoles de communication. On les catégorise en 4 couches différentes.

#.  La **couche physique** c'est à dire les interfaces réseaux.
#.  La **couche internet** qui relie de façon logique des machines dans un même réseau.
#.  La **couche transport** qui assure l'acheminement des données sous forme de paquets et qui vérifie que les données sont correctement arrivées. 
#.  La **couche application** qui est l'application logicielle à l'origine de l'échange des données sur un réseau.

.. note::

    Un paquet de données traverse les 4 couches différentes avant de traverser le réseau jusqu'au destinataire.
    Pour chaque couche, des informations sont ajoutées au paquet de données : c’est l’encapsulation des données.

.. rubric:: Questions

#.  Au niveau de la couche physique, comment une interface réseau est-elle identifiée de façon unique ?
#.  Au niveau de la couche internet, comment sont identifiées 2 machines appartenant à un même réseau ?
#.  Citer 2 protocoles de la couche transport pour les échanges de données sur internet. 
#.  Citer des applications qui échangent des données sur internet ou dans un réseau.

Un réseau internet
-------------------

Le logiciel filius permet de créer et simuler le fonctionnement de réseaux interconnectés.

Voici 2 réseaux ``L1`` et ``L2``:

.. figure:: ../img/act_reseau_0.png
    :alt: image
    :align: center
    :width: 560

-   Le réseau ``L1`` comprend 1 portable qui a pour adresse IP ``192.168.1.10``.
-   Le réseau ``L2`` comprend 1 serveur qui a pour adresse IP ``192.168.6.10``.

.. note::

    Le réseau ci-dessous est contenu dans un fichier à télécharger : :download:`../filius/activite_reseau.fls`.

.. rubric:: Étude du réseau

#.  Comment s'appelle les appareils ``R1`` à ``R6`` qui relient les réseaux ``L1`` et ``L2`` ?
#.  Combien d'interfaces réseau contient ``R3`` ? Donner l'adresse réseau de chacune de ses interfaces.
#.  Reproduire le schéma de réseau simplifié en indiquant les adresses réseaux.
#.  La commande ``ping ip_machine_distante`` permet de vérifier la connexion avec une autre machine du réseau. Tester cette commande entre le portable et le serveur.
#.  La commande ``traceroute ip_machine_distante`` affiche la route suivi par les paquets envoyés entre deux machines. Quels routeurs sont traversés dans une communication entre le portable du réseau ``L1`` et le serveur du réseau ``L2`` ? 
#.  Ajouter un PC portable dans le réseau ``L1`` et le configurer pour qu’il communique avec toutes les machines du réseau ``L1`` et le serveur du réseau ``L2``.
#.  Ajouter un câble entre les routeurs ``R3`` et ``R5`` en donnant des adresses valides aux interfaces. Quelle est la route pour atteindre le serveur du réseau ``L2`` depuis un PC du réseau ``L1`` ?