Activité : Tables de routage
=============================

Les routeurs sont des appareils munis de plusieurs **interfaces** réseaux qui permettent aux routeurs de communiquer entre eux et d'acheminer les paquets de données. Les interfaces sont désignées par leur nom ou par leur adresse IP.

Une **passerelle** est un routeur ou une interface du routeur qui permet d’accéder au réseau de destination.

Sur la figure ci-dessous, les réseaux L1 et L2 sont reliés par différents routeurs.

.. image:: ../img/reseau_complet.png
    :alt: image
    :align: center
    :width: 100%

Chaque routeur dispose d’une table de routage qui contient les différents réseaux auxquels ils peut accéder, les passerelles à utiliser pour y accéder et les interfaces réseaux à utiliser.

.. table::
    :align: center
    :class: border-on border-width-thin

    +-------------------------+---------------+
    | destination             | passerelle    |
    +-------------------------+---------------+
    | IP destination / masque | routeur ou IP |
    +-------------------------+---------------+

Au démarrage du réseau, après la mise sous tension, les tables de routage sont vides et chaque routeur commence par identifier ses voisins immédiats et complète sa table.

Après un certain temps, les routeurs échangent leurs informations et les tables se stabilisent.

#.  Au démarrage, la table de routage du routeur ``R1`` est vide. Compléter sa table avec ses routeurs voisins immédiats.

    .. table::
        :align: center
        :class: border-on border-width-thin

        +-------------+---------------+
        | destination | passerelle    |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+

#.  Au démarrage, la table de routage du routeur ``R3`` est vide aussi. Compléter sa table avec ses routeurs voisins immédiats.

    .. table::
        :align: center
        :class: border-on border-width-thin

        +-------------+---------------+
        | destination | passerelle    |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+

#.  Les routeurs ``R1`` et ``R3`` échangent leurs tables. Compléter la table de routage du routeur ``R1`` avec les nouvelles informations obtenues de ``R3``

    .. table::
        :align: center
        :class: border-on border-width-thin

        +-------------+---------------+
        | destination | passerelle    |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+

#.  Après plusieurs échanges entre les routeurs, les tables de routage se stabilisent. Compléter la table de routage du routeur ``R1`` une fois stabilisée.

    .. table::
        :align: center
        :class: border-on border-width-thin

        +-------------+---------------+
        | destination | passerelle    |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+
        |             |               |
        +-------------+---------------+

