Au baccalauréat
===============

Exercices type bac sur les réseaux et protocoles de routage.

.. exercice::

    Exercice bac métropole session 2021 : :download:`exercice 3 - sujet 1 <../doc/21-NSIJ1ME1_ex3.pdf>`

.. exercice::

    Exercice bac métropole session 2021 : :download:`exercice 5 - sujet 2 <../doc/21_NSIJ1ME5_ex5.pdf>`
