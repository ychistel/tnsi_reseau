.. TNSI
   
Réseaux et protocoles
=====================

.. image:: img/intro_reseau.jpeg
    :align: center
    :width: 100%
    :class: margin-bottom-16

Pour démarrer, quelques rappels sur les notions vues en classe de première.

-   Transmission de données dans un réseau
-   Protocoles de communication
-   Architecture d’un réseau

Ensuite, identifer, suivant le protocole de routage utilisé, la route empruntée par un paquet. Les protocoles de routages étudiés sont:

-   Le protocole RIP (Routing Information Protocol)
-   Le protocole OSPF (Open Shortest Path First)

.. toctree::
    :maxdepth: 1
    :hidden:
    
    content/activite_reseau.rst
    content/architecture_reseau.rst
    content/exercice_1.rst
    content/activite_routage.rst
    content/protocole.rst
    content/exercice_2.rst
    content/exercice_3.rst
